const COLORS = {
  red: "rgb(255, 0, 0)",
  black: "rgb(0, 0, 0)",
  invalidParkingColor: "rgb(98, 0, 238)",
  defaultParkingColor: "rgb(0, 252, 255)",
};

const WIDTH_LINE = {
  small: 2,
  medium: 5,
};

const TITLE = {
  getImage: "Адрес получения картинки",
  getMarkup: "Адрес получения разметки",
  postMarkup: "Адрес отправки разметки",
};

const URL = {
  getImage: "http://gpu3-chel1:8083/photo",
  getMarkup: "http://10.80.0.147:5002/marking/v2",
  postMarkup: "http://10.80.0.147:5002/marking/v2",
};

const MOVE_TYPE = {
  delta: "delta",
  point: "point",
};

const ACTION_TYPES = {
  drawLine: "drawLine",
  addPoint: "addPoint",
  moveLine: "moveLine",
  movePoint: "movePoint",
  moveDelta: "moveDelta",
  downPoint: "downPoint",
  waitAction: "waitAction",
  selectedLine: "selectedLine",
  pointerPoint: "pointerPoint",
};

export { COLORS, WIDTH_LINE, URL, TITLE, MOVE_TYPE, ACTION_TYPES };
