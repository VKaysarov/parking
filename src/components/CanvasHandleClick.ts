import { ACTION_TYPES } from "./Constants";

const addPointOnLine = (self: any, x: number, y: number): void => {
  const { indexLine, indexPoint } = self.lineover(x, y);
  const { points } = self.lines[indexLine].main_line;
  const point = {
    id: points.length,
    x,
    y,
  };

  points.splice(indexPoint, 0, point);

  self.lines[indexLine].main_line.points = points;
  self.$store.dispatch("savePoint", self.lines);
  self.$store.dispatch("banReload");
};

const selectPointOnLine = (self: any, x: number, y: number): void => {
  const { indexPoint, indexLine } = self.pointover(x, y);
  const { main_line } = self.lines[indexLine];

  main_line.attributes.selected = true;

  self.indexStartPoint = indexPoint;
  self.indexSelectedLine = indexLine;
  self.$store.dispatch("changeAction", ACTION_TYPES.selectedLine);
};

const drawLine = (self: any, x: number, y: number): void => {
  const { lines } = self;
  const { points } = self.lines[lines.length - 1].main_line;
  const point = {
    id: points.length,
    x,
    y,
  };

  points.push(point);
  self.indexStartPoint++;
  self.lines[lines.length - 1].main_line.points = points;
  self.$store.dispatch("banReload");
  self.$store.dispatch("savePoint", lines);
};

const dischargeSelectedLine = (self: any): void => {
  for (const line of self.lines) {
    line.main_line.attributes.selected = false;
  }
  self.$store.dispatch("selectLine", -1);
  self.$store.dispatch("changeAction", ACTION_TYPES.waitAction);
};

const startDraw = (self: any, x: number, y: number): void => {
  const { lines } = self;
  const line = {
    main_line: {
      points: [
        {
          id: 0,
          x,
          y,
        },
      ],
      delta: {
        x: 50,
        y: 50,
      },
      attributes: {
        parking_size: 1,
        disabled: false,
        selected: true,
        path: {} as Path2D,
      },
    },
  };
  lines.push(line);
  self.indexStartPoint = 0;
  self.$store.dispatch("banReload");
  self.$store.dispatch("savePoint", lines);
  self.$store.dispatch("changeAction", ACTION_TYPES.drawLine);
};

export {
  drawLine,
  startDraw,
  addPointOnLine,
  selectPointOnLine,
  dischargeSelectedLine,
};
