import { ACTION_TYPES } from "./Constants";
import { renderLine } from "./CanvasRender";

const animationDrawingLine = (self: any, x: number, y: number): void => {
  const canvas = self.$refs.canvas as HTMLCanvasElement;
  const ctx = canvas.getContext("2d") as CanvasRenderingContext2D;
  const { main_line } = self.lines[self.lines.length - 1];
  const start = main_line.points[self.indexStartPoint];

  canvas.width = canvas.offsetWidth;
  canvas.height = canvas.offsetHeight;
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  renderLine(ctx, start.x, start.y, x, y);
};

const dragPoint = (self: any, x: number, y: number): void => {
  const { lines, indexSelectedLine, indexMovePoint } = self;

  // Сохранение изменения координат точки
  lines[indexSelectedLine].main_line.points[indexMovePoint].x = x;
  lines[indexSelectedLine].main_line.points[indexMovePoint].y = y;

  self.$store.dispatch("savePoint", lines);
  self.$store.dispatch("changeAction", ACTION_TYPES.movePoint);
};

const dragDelta = (self: any, x: number, y: number): void => {
  const { lines } = self;
  const { main_line } = lines[self.indexSelectedLine];
  const point = main_line.points[self.indexDeltaPoint];

  const delta = {
    x: point.x - x,
    y: point.y - y,
  };

  main_line.delta = delta;
  self.$store.dispatch("savePoint", lines);
  self.$store.dispatch("changeAction", ACTION_TYPES.moveDelta);
};

const dragLine = (self: any, x: number, y: number): void => {
  // Отнимаем предыдущие координаты мыши из текущего положения,
  // тем самым определяя куда движется мышь (верх/низ, право/лево)
  const subX = x - self.moveLine.x; // Выводит -1 или +1
  const subY = y - self.moveLine.y;
  const { main_line } = self.lines[self.moveLine.index];

  // Прибавляем результат к каждой точке
  for (const point of main_line.points) {
    point.x += subX;
    point.y += subY;
  }

  // Сохраняем координаты для следущей итерации
  self.moveLine.x = x;
  self.moveLine.y = y;
  self.$store.dispatch("changeAction", ACTION_TYPES.moveLine);
};

export { animationDrawingLine, dragPoint, dragDelta, dragLine };
