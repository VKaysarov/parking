import { WIDTH_LINE } from "./Constants";

const renderMainLine = (
  ctx: CanvasRenderingContext2D,
  points: IPointCoordinate[]
): void => {
  ctx.lineWidth = WIDTH_LINE.medium;
  ctx.beginPath();
  ctx.strokeStyle = "black";
  for (let i = 0; i < points.length; i++) {
    const { x, y } = points[i];
    const circle = new Path2D();
    ctx.fillStyle = "blue";
    circle.arc(x, y, 5, 0, 2 * Math.PI);
    ctx.fill(circle);
    ctx.lineTo(x, y);
  }
  ctx.stroke();
}

const renderAreaLine = (
  self: any,
  ctx: CanvasRenderingContext2D,
  mainLine: IMainLine
): void => {
  const path = new Path2D();
  const { disabled } = mainLine.attributes;
  const { defaultParkingColor, invalidParkingColor } = self;

  mainLine.attributes.path = path;

  for (let i = 0; i < mainLine.points.length; i++) {
    const end = mainLine.points[i];
    const x = end.x - mainLine.delta.x;
    const y = end.y - mainLine.delta.y;

    ctx.globalAlpha = 0.5;
    ctx.fillStyle = disabled ? defaultParkingColor : invalidParkingColor;
    path.lineTo(x, y);
  }

  for (let i = mainLine.points.length - 1; i >= 0; i--) {
    const end = mainLine.points[i];
    const x = end.x + mainLine.delta.x;
    const y = end.y + mainLine.delta.y;

    path.lineTo(x, y);
  }

  ctx.fill(path);
  ctx.globalAlpha = 1;
}

const renderDelta = (
  self: any,
  ctx: CanvasRenderingContext2D,
  mainLine: IMainLine
): void => {
  const per = {
    lenMainLine: 0,
    remainderStep: 0,
  };
  //         B(400, 100)
  //              ^
  //             / \
  //            /   \
  //           /     \
  //          /       \
  // A(100, 500)      C(700, 500)
  //
  //
  // AB(-300, 400), BC(-300, -400)
  //           _________________
  // AB, BC = √(-300)² + (-400)² = 500
  // ABC = AB + BC = 1000
  // ABCstep = 1000 / 5 = 200
  // ABcount, BCcount = 500 / 200 = 2,5;
  // ABstep(-120, 160), BCstep(-120, -160)
  //
  // AsupX1 = 100 - (-120) * 1 = 100 + 120 * 1 = 220
  // AsupY1 = 500 - 160 * 1 = 340
  // Asup1(220, 340)
  // RemainX = 220 - 400 = 180 > 120 => RemainX = 0
  // RemainY = 340 - 100 = 240 > 160 => RemainY = 0
  //
  // AsupX2 = 100 - (-120) * 2 = 100 + 120 * 2 = 340
  // AsupY2 = 500 - 160 * 2 = 180
  // Asup2(340, 180)
  // RemainX = 340 - 400 = 60 < 120 => RemainX = -60
  // RemainY = 180 - 100 = 80 < 120 => RemainY = 80
  //
  // BsupX1 = 400 - (-120) * 1 = 400 + 120 * 1 = 520
  // BsupX1 = BsupX1 + RemainX = 520 - 60 = 460
  // BsupY1 = 100 - (-160) * 1 = 100 + 160 * 1 = 260
  // BsupY1 = BsupY1 + RemainY = 260 + 80 = 340
  // Bsup1(460, 340)
  // RemainX = 460 - 700 = 240 > 120 => RemainX = 0
  //
  // BsupX2 = 400 - (-120) * 2 = 400 + 120 * 2 = 640
  // BsupY2 = 100 - (-160) * 2 = 100 + 160 * 2 = 420
  // Bsup2(640, 420)
  //        _________________________
  // 100 = √(400 - x2)² + (100 - y2)²
  // 100 / 500 = 0,2
  // Cx = 400 + (700 - 400) * 0,2
  // Cy = 100 + (500 - 100) * 0,2


  for (let i = 0; i < mainLine.points.length - 1; i++) {
    const leftPoint = mainLine.points[i].x;
    const rightPoint = mainLine.points[i + 1].x;
    const topPoint = mainLine.points[i].y;
    const bottomPoint = mainLine.points[i + 1].y;
    const lenLineX = leftPoint - rightPoint;
    const lenLineY = topPoint - bottomPoint;
    const lenLine = getLenLine(lenLineX, lenLineY);
    per.lenMainLine += lenLine;
  }

  for (const [index, point] of mainLine.points.entries()) {
    const circle = new Path2D();
    const { x, y } = point;
    const delta = {
      x: x - mainLine.delta.x,
      y: y - mainLine.delta.y,
    };
    const reverseDelta = {
      x: x + mainLine.delta.x,
      y: y + mainLine.delta.y,
    };

    renderSupportLine(index, per, ctx, mainLine);

    ctx.lineWidth = WIDTH_LINE.small;
    ctx.fillStyle = "blue";
    ctx.strokeStyle = "chartreuse";

    // Рисование основной линии дельты
    renderLine(ctx, x, y, delta.x, delta.y);

    // Рисование точки дельты
    circle.arc(delta.x, delta.y, 5, 0, 2 * Math.PI);
    ctx.fill(circle);

    // Отрисовка отзеркаленой линии дельты
    renderLine(ctx, x, y, reverseDelta.x, reverseDelta.y);

    // Рисование точки для отзеркаленой дельты
    circle.arc(reverseDelta.x, reverseDelta.y, 5, 0, 2 * Math.PI);
    ctx.fill(circle);
  }
}

const renderLine = (
  ctx: CanvasRenderingContext2D,
  startX: number,
  startY: number,
  endX: number,
  endY: number
): void => {
  ctx.beginPath();
  ctx.moveTo(startX, startY);
  ctx.lineTo(endX, endY);
  ctx.stroke();
}

const renderSupportLine = (
  index: number,
  per: any,
  ctx: CanvasRenderingContext2D,
  { points, delta, attributes }: IMainLine
) => {
  // Выйти из функции если это последняя точка
  if (index + 1 === points.length) {
    return;
  }

  const leftPoint = points[index].x;
  const rightPoint = points[index + 1].x;
  const topPoint = points[index].y;
  const bottomPoint = points[index + 1].y;

  // Получение длины линии
  const lenLineX = leftPoint - rightPoint;
  const lenLineY = topPoint - bottomPoint;
  const lenLine = getLenLine(lenLineX, lenLineY);

  // Получение шага вспомогательной линии
  const stepMainLine = per.lenMainLine / attributes.parking_size;
  const countSupportLine = lenLine / stepMainLine;
  const supportLine = new Array(Math.ceil(countSupportLine));
  const stepSupPointX = lenLineX / countSupportLine;
  const stepSupPointY = lenLineY / countSupportLine;

  // Вычисление координаты вспомогательной точки
  const lenRightRemainder = stepMainLine - per.remainderStep;
  const k = lenRightRemainder / lenLine;
  const supX = leftPoint + (rightPoint - leftPoint) * k;
  const supY = topPoint + (bottomPoint - topPoint) * k;

  for (let i = 1; i < supportLine.length; i++) {
    const supPointX = supX - stepSupPointX * (i - 1);
    const supPointY = supY - stepSupPointY * (i - 1);
    const startX = supPointX - delta.x;
    const startY = supPointY - delta.y;
    const endX = supPointX + delta.x;
    const endY = supPointY + delta.y;

    ctx.lineWidth = WIDTH_LINE.medium;
    ctx.strokeStyle = "rgba(255, 255, 255, 0.5)";
    renderLine(ctx, startX, startY, endX, endY);

    if (i + 1 === supportLine.length) {
      per.remainderStep = getLenLine(
        supPointX - rightPoint,
        supPointY - bottomPoint
      );
    }
  }
};

// Строим треугольник с катетами X и Y,
// и вычисляем гипотенузу по теореме Пифагора
const getLenLine = (legX: number, legY: number) =>
  Math.sqrt(Math.pow(legX, 2) + Math.pow(legY, 2));

export { renderMainLine, renderAreaLine, renderDelta, renderLine };
