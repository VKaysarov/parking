import { createStore } from "vuex";
import { URL, TITLE, ACTION_TYPES } from "../components/Constants";

export default createStore({
  state: {
    reload: true,
    selectedLine: 0,
    action: ACTION_TYPES.waitAction,
    lines: [] as parkingPlacesArrayType,
    imgId: localStorage.getItem("imgId") || 100,
    urlList: [
      {
        title: TITLE.getImage,
        url: URL.getImage,
      },
      {
        title: TITLE.getMarkup,
        url: URL.getMarkup,
      },
      {
        title: TITLE.postMarkup,
        url: URL.postMarkup,
      },
    ],
  },
  mutations: {
    SAVE_POINT(state, lines) {
      state.lines = lines;
    },
    SELECT_LINE(state, index) {
      state.selectedLine = index;
    },
    CHANGE_ACTION(state, action) {
      state.action = action;
    },
    CHANGE_IMG_ID(state, id) {
      state.imgId = id;
      localStorage.setItem("imgId", `${state.imgId}`);
    },
    CHANGE_URL(state, { index, url }) {
      state.urlList[index].url = url;
    },
    GET_MARKUP(state, result) {
      state.selectedLine = -1;
      state.lines = result;
      state.action = ACTION_TYPES.waitAction;
    },
    BAN_RELOAD(state) {
      state.reload = false;
    },
    ALLOW_RELOAD(state) {
      state.reload = true;
    },
  },
  actions: {
    savePoint(context, lines) {
      context.commit("SAVE_POINT", lines);
    },
    selectLine(context, index) {
      context.commit("SELECT_LINE", index);
    },
    changeAction(context, action) {
      context.commit("CHANGE_ACTION", action);
    },
    changeImgId(context, id) {
      context.commit("CHANGE_IMG_ID", id);
      context.dispatch("getMarkup");
    },
    changeUrl(context, { index, url }) {
      context.commit("CHANGE_URL", { index, url });
    },
    async getMarkup({ state, commit }) {
      const response = await fetch(`${state.urlList[1].url}/${state.imgId}`);
      const result = await response.json();

      if (response.ok) {
        commit("GET_MARKUP", result);
      }
    },
    banReload(context) {
      context.commit("BAN_RELOAD");
    },
    allowReload(context) {
      context.commit("ALLOW_RELOAD");
    },
  },
  modules: {},
});
