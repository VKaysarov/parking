interface IAttributesMainLine {
  parking_size: number;
  disabled: boolean;
  selected: boolean;
  path: Path2D;
}

interface ICoordinates {
  x: number;
  y: number;
}

interface IPointCoordinate extends ICoordinates {
  id: number;
}

interface IMainLine {
  points: IPointCoordinate[];
  delta: ICoordinates;
  attributes: IAttributesMainLine;
}

interface ILines {
  main_line: IMainLine;
}

type parkingPlacesArrayType = ILines[];

interface ICamera {
  id: number;
  parking_name: string;
}
